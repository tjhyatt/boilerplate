
//* redactor =================================================== */

if (!RedactorPlugins) var RedactorPlugins = {};

// toggle button

RedactorPlugins.toggleButton = function()
{
    return {
        init: function()
        {
            var button = this.button.addAfter('link', 'toggle-button', 'Toggle Button');

            this.button.setIcon(button, '<i class="fa fa-square"></i>');

            this.button.addCallback(button, function()
            {
                var $links = this.link.get();

                $links.each(function(){
                    $(this).toggleClass('btn');
                })

                this.selection.restore();
            });
        }
    };
};

// subscript and superscript

RedactorPlugins.scriptbuttons = function()
{
    return {
        init: function()
        {
            var sub = this.button.addAfter('italic', 'subscript', 'x₂');
            var sup = this.button.addAfter('italic', 'superscript', 'x²');

            this.button.addCallback(sup, this.scriptbuttons.formatSup);
            this.button.addCallback(sub, this.scriptbuttons.formatSub);
        },
        formatSup: function()
        {
            this.inline.format('sup');
        },
        formatSub: function()
        {
            this.inline.format('sub');
        }
    };
};