function initMap() {
    var myLatLng = {lat: -38.0775695, lng: 145.2511191};

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: myLatLng,
        zoomControl: false,
        scaleControl: false,
        scrollwheel: false,
        disableDoubleClickZoom: true,
        styles: [
          {
            "featureType":"all",
            "elementType":"labels.text.fill",
            "stylers":[
              {"saturation":100},
              {"color":"#FFFFFF"},
              {"lightness":0}
            ]
          },{
            "featureType":"all",
            "elementType":"labels.text.stroke",
            "stylers":[
              {"visibility":"on"},
              {"color":"#000000"},
              {"lightness":0}
            ]
          },{
            "featureType":"all",
            "elementType":"labels.icon",
            "stylers":[
              {"visibility":"off"}
            ]
          },{
            "featureType":"administrative",
            "elementType":"geometry.fill",
            "stylers":[
              {"color":"#000000"},
              {"lightness":20}
            ]
          },{
            "featureType":"administrative",
            "elementType":"geometry.stroke",
            "stylers":[
              {"color":"#000000"},
              {"lightness":17},
              {"weight":1.2}
            ]
          },{
            "featureType":"landscape",
            "elementType":"geometry",
            "stylers":[
              {"color":"#000000"},
              {"lightness":30}
            ]
          },{
            "featureType":"poi",
            "elementType":"geometry",
            "stylers":[
              {"color":"#000000"},
              {"lightness":35}
            ]
          },{
            "featureType":"road.highway",
            "elementType":"geometry.fill",
            "stylers":[
              {"color":"#f19202"},
              {"lightness":45}
            ]
          },{
            "featureType":"road.highway",
            "elementType":"geometry.stroke",
            "stylers":[
              {"color":"#f19202"},
              {"lightness":45},
              {"weight":0.2}
            ]
          },{
            "featureType":"road.arterial",
            "elementType":"geometry",
            "stylers":[
              {"color":"#f0e401"},
              {"lightness":56}
            ]
          },{
            "featureType":"road.local",
            "elementType":"geometry",
            "stylers":[
              {"color":"#f0e401"},
              {"lightness":56}
            ]
          },{
            "featureType":"transit",
            "elementType":"geometry",
            "stylers":[
              {"color":"#000000"},
              {"lightness":19}
            ]
          },{
            "featureType":"water",
            "elementType":"geometry",
            "stylers":[
              {"color":"#000000"},
              {"lightness":17}
            ]
          }
        ]
    });

    var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="firstHeading" class="firstHeading">Cocobean <small>Desserts & Cafe</small></h1>'+
      '<div id="bodyContent">'+
      '<p><b>Opening Hours</b><br />' +
      'Monday: <br />'+
      'Tuesday: <br />'+
      'Wednesday: <br />'+
      'Thursday: <br />'+
      'Friday: <br />'+
      'Saturday: <br />'+
      'Sunday: <br />'+
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Cocobean'
  });

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

}
