//* validate ============================== */

//* ajax submit ============================== */

$('.btn-submit').click(function(ev) {
  // Prevent the form from actually submitting
  ev.preventDefault();

  var $form = $(this).closest('form');
  var data = $form.serialize();
  $form.parent().addClass('s_submitting');

  // Send it to the server
  $.post('/wheelform/message/send',
    data,
    function(response) {
      if (response.success) {
        // response.message is the message saved in the Form Settings
        $('#thanks').fadeIn();
        $form.parent().removeClass('s_submitting');
      } else {
        // response.values will contain user submitted values
        // response.errors will be an array containing any validation errors that occurred, indexed by field name
        // e.g. response.error['email'] => ['Email is required', 'Email is invalid']
        alert('An error occurred. Please try again.');
        $form.parent().removeClass('s_submitting');
      }
    }
  );
});