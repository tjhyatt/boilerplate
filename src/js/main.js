$(document).ready(function() {

  // On menu click
  $('.nav-button-holder').click(function() {
    $('html,body').scrollTop(0);
    $('.mobile-menu').toggleClass('s-active');
    $('.nav-button').toggleClass('s-active');
    $('body').toggleClass('s-stop-scroll');
  });

  // Add class if desktop size on page load
  if ($(window).width() > 768) {
    $('.nav').addClass('l-fixed');
  }

  // Detect screen width
  $(window).resize(function() {
    var win = $(window).width();

    if (win > 768) {
      $('.mobile-menu').removeClass('s-active');
      $('.nav-button').removeClass('s-active');
      $('body').removeClass('s-stop-scroll');
      $('.nav').addClass('l-fixed');
    }

    if (win <= 768) {
      $('.nav').removeClass('l-fixed');
    }
  });

});
